//console.log("Hola Mundo");
var express = require('express');//paquete de dependecias del NODE
var userFile = require('./data/user.json'); //dependencia para manipular archivos
var bodyParse = require('body-parser'); //biblioteca para poder trabajar con body en el request de un post
var app = express();
app.use(bodyParse.json());
var totalUsers = 0;
const URL_BASE = '/apitechu/v1/'; //Constantes con mayùscula
const PORT = process.env.PORT || 3000;


//Primer parametro: URI
//Segundo parametro: Callback (funciòn dentro de otra funciòn)

// Peticiòn GET de todos los 'users'
app.get(URL_BASE + 'users',
      function(request,response) {
        console.log('GET' + URL_BASE + 'users');
        response.send(userFile);
        //response.send('Buenos dìas Lima'); //Response de Prueba
});//LA coma no es necesario para que funcione la sentencia.

// Peticiòn GET de un 'users' en especifico
app.get(URL_BASE + 'users/:id',
         function(request,response){
           console.log('GET' + URL_BASE + 'users/id');
           let indice = request.params.id;
           let instancia = userFile[indice - 1];
           console.log(instancia);
           //*Condicional lineal
           let respuesta = instancia != undefined ? instancia : {"mensaje":"Recurso no encontrado"};
           response.status (222); //Còdigo de Error HTTP
           response.send(respuesta);

           //*Condicional con IF
           //if (instancia != undefined)
           //  response.send(instancia);
           //else {
           //    response.send({"mensaje":"Recurso no encontrado"});
           //}

           //console.log(request.params.id);
           //console.log(request.params);
});

//* GET con Query String
app.get(URL_BASE + 'usersq',
         function(request,response){
           console.log('GET' + URL_BASE + ' con query string');
           console.log(request.query);
           response.send(respuesta);
});

//Peticiòn POST
app.post(URL_BASE + 'users',
           function(req,res){
           totalUsers = userFile.length;
          //*condicional para asegurarnos que nos envìen datos
           let cuerpo = Object.keys(req.body).length;
           //let respuesta = (instancia >0 undefined) ? userFile.push(newUser) : {"mensaje":"Recurso no encontrado"};
           console.log(cuerpo);
           let newUser = {
                  userId : totalUsers + 1,
                  first_name : req.body.first_name,
                  last_name : req.body.last_name,
                  email : req.body.email,
                  password : req.body.password
            }
            var mensaje="Usuario creado con èxito.";
            (cuerpo > 0) ? userFile.push(newUser) : mensaje = {"mensaje":"No existe Body"};
            writeUserDataToFile(userFile);
                //userFile.push(newUser);
                res.status(200);
                res.send({"Mensaje" : mensaje,
                          "Usuarios": JSON.stringify(userFile)});
                //res.send({"mensaje" : "Usuario creado con èxito.","Usuario" : newUser});
});


//put correcto
app.put(URL_BASE + 'users/:id',
   function(req,res){
     var cntBody = Object.keys(req.body).length;
     if (cntBody > 0) {
       let idx = req.params.id - 1
       if(userFile[idx] != undefined){
         userFile[idx].first_name = req.body.first_name;
         userFile[idx].last_name = req.body.last_name;
         userFile[idx].email = req.body.email;
         userFile[idx].password = req.body.password;
         writeUserDataToFile(userFile);
         res.status(201);
         res.send({"mensaje" : "Usuario actualizado con exito.",
                   "Array" : userFile[idx]});
       }
       else{
         res.send({"mensaje":"Usuario no encontrado."})
       }
     }
     else {
       let varError = {"mensaje" : "No Existe parametros para Body"};
       res.send(varError)
       console.log(varError);
     }
   });

//Peticiòn DELETE
app.delete(URL_BASE + 'users/:id',
   function(req,res){
       let idx = req.params.id - 1
       if(userFile[idx] != undefined){
         userFile.splice(idx,1);
         writeUserDataToFile(userFile);
         res.status(200);
         res.send({"mensaje" : "Usuario eliminado con exito.",
                   "Array" : JSON.stringify(userFile)});
       }
       else{
         res.send({"mensaje":"Usuario no encontrado."})
       }
});

//LOGIN

//Peticiòn LOGIN
app.post(URL_BASE + 'users/login',
           function(req,res){
           var cuerpo = Object.keys(req.body).length;
           var email = req.body.email;
           var pass = req.body.password;
           var encontrado = false;
           if (cuerpo > 0 && cuerpo == 2)
           {
             for(user of userFile)
             {
                 if(user.email == email){
                    if (user.password == pass){
                        user.logged = true
                        encontrado = true;
                        writeUserDataToFile(userFile);
                        res.status(200)
                        break;
                      }
                  }
              }
              var valor ={
                "Mensaje" : (encontrado == true) ?  "Login correcto" : "Login Incorrecto",
                "Usuarios" : (encontrado == true) ? JSON.stringify(userFile) : ""
              }
               res.send(valor);
           }
           else {
              res.status(222);
              res.send({"Mensaje" : "No Existe parametros para Body"});
           }
});


//POST DesLogueo con Body
app.post(URL_BASE + 'users/unlogin',
      function(req,res){
        var cuerpo = Object.keys(req.body).length;
        var msgLogueado = "";
        if (cuerpo > 0 && cuerpo == 2) {
          var encontrado = false;
          for(user of userFile){
            if (user.email == req.body.email && user.password == req.body.password) {
              if(user.logged != undefined)
              {
                delete user.logged;
                writeUserDataToFile(userFile);
                encontrado = true;
                break;
              }else{msgLogueado = "- Usuario no se encuentra logueado"
                  break;
              };
           }
         }
         var valor ={
           "Mensaje" : (encontrado == true) ?  "logout correcto" : "logout Incorrecto" + msgLogueado,
           "Usuarios" : (encontrado == true) ? JSON.stringify(userFile) : ""
         }
          res.send(valor);
         } else {
          let varError = {"mensaje" : "No Existe parametros para Body"};
          res.send(varError);
          res.status(222);
        }
       });


//POST DesLogueo con Uri
  app.post(URL_BASE + 'users/unlogin/:id',
             function(req,res){
               var id = req.params.id;
               var msgLogueado = "";
               if (id != undefined) {
                 var encontrado = false;
                 for(user of userFile){
                   if (user.userID == id) {
                     if(user.logged != undefined)
                     {
                       delete user.logged;
                       writeUserDataToFile(userFile);
                       encontrado = true;
                       break;
                     }else{msgLogueado = "- Usuario no se encuentra logueado"
                         break;
                     };
                  }
                }
                 var valor ={
                   "Mensaje" : (encontrado == true) ?  "logout correcto" : "logout Incorrecto" + msgLogueado,
                   "Usuarios" : (encontrado == true) ? JSON.stringify(userFile) : ""
                 }
                  res.send(valor);

                } else {
                 let varError = {"mensaje" : "No Existe parametros para Body"};
                 res.send(varError);
                 res.status(222);
               }
});


// Peticiòn GET para Obtener usuarios logueados
app.get(URL_BASE + 'usersLogados',
      function(req,res) {
        var userLogged = [];
        for(user of userFile)
        {
            if(user.logged != undefined)
              userLogged.push(user);
        }
        res.status(201);
        res.send(userLogged);

});

// Peticiòn GET para Obtener un rango de usuarios - QUERYSTRING
app.get(URL_BASE + 'usersRango',
      function(req,res) {
        var userLogged = [];
        var msg="";
        if (req.query.idFin <= userFile.length)
        {
          if (req.query.idIni < req.query.idFin)
          {
            for (var i = req.query.idIni; i <= req.query.idFin; i++) {
              userLogged.push(userFile[i-1]) ;
            }
          }else msg = "El idIni no puede ser mayor al idFin";
        }else msg = "El IdFin no puede ser mayor a la longitud del Array";

        res.status(201);
        res.send({"Mensaje:" : msg,
                  ":Array:" : userLogged});
});

// Peticiòn GET para Obtener usuarios logueados - QUERYSTRING
app.get(URL_BASE + 'usersLogadosq',
      function(req,res) {
        var userLogged = [];
        for(user of userFile)
        {
            if (req.query.idIni <= user.userID && user.userID <= req.query.idFin){
                if(user.logged != undefined)
                  userLogged.push(user);
            }
        }
        console.log(req.query.idIni + "/" + req.query.idFin);
        res.status(201);
        res.send(userLogged);
});

function writeUserDataToFile(data) {
   var fs = require('fs');
   var jsonUserData = JSON.stringify(data);

   fs.writeFile("./data/user.json", jsonUserData, "utf8",
    function(err) { //función manejadora para gestionar errores de escritura
      if(err) {
        console.log(err);
      } else {
        console.log("Datos escritos en 'users.json'.");
      }
    })
 }


//Puerto por donde escucha el servidor. Por convenciòn 3000
app.listen(3000, function() {
  console.log('*Empezamos Sesiòn 6***TechU Practitioner*');
});

//app.listen(PORT);
//console.log('API TechU escuchando por el puerto 3000');
